/*
export function someAction (context) {
}
*/
export const add_ingredient_to_shopping = ({commit},ingredient) => {
    let ingredientObject = { 'name': ingredient.name, 'quantity': ingredient.quantity ,'shopping':true};
    localStorage.object = JSON.stringify(ingredientObject);
    ingredientObject = JSON.parse(localStorage.object);
    console.log(ingredientObject);
    commit("add_to_shopping",ingredientObject);


};
export const delete_ingredient_from_shopping = ({commit} ,ingredient) => {
    console.log("delete ingredient");
    commit("deleteIngredient",ingredient);
};
export const edit_quantity= ({commit} ,ingredient) => {
    let ingredientObject = { 'name': ingredient.name, 'quantity': ingredient.quantity };
    localStorage.object = JSON.stringify(ingredientObject);
    ingredientObject = JSON.parse(localStorage.object);
    console.log(ingredientObject);
    commit("editIngredient",ingredientObject);
};