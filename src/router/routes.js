import AuthLayout from 'layouts/AuthLayout'
import MainLayout from 'layouts/MainLayout'
const routes = [
  {
    path: '/',
    redirect: 'register',
    component: MainLayout,
    children: [
      {
        path: '/recipes',
        name: 'recipes',
        component: () => import(/* webpackChunkName: "demo" */ 'pages/recipes.vue')
      },
      {
        path: '/add_recipe',
        name: 'add_recipe',
        component: () => import(/* webpackChunkName: "demo" */ 'pages/add_recipe.vue')
      },
      {
        path: '/edit_recipe/:id',
        name: 'edit_recipe',
        component: () => import(/* webpackChunkName: "demo" */ 'pages/edit_recipe.vue')
      },
      {
        path: '/shopping_list',
        name: 'shopping_list',
        component: () => import(/* webpackChunkName: "demo" */ 'pages/shopping_list.vue')
      },
    ]
  },
  {
    path: '/',
    redirect: 'register',
    component: AuthLayout,
    children: [
      {
        path: '/login',
        name: 'login',
        component: () => import(/* webpackChunkName: "demo" */ 'pages/login.vue')
      },
      {
        path: '/register',
        name: 'register',
        component: () => import(/* webpackChunkName: "demo" */ 'pages/register.vue')
      },

    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  },

  ]

export default routes
